<?php

add_action( 'init', 'register_jhi_locations_taxonomy', 0 );

// Register Custom Taxonomy
function register_jhi_locations_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Locations', 'Taxonomy General Name', 'jhi' ),
		'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'jhi' ),
		'menu_name'                  => __( 'Locations', 'jhi' ),
		'all_items'                  => __( 'All Locations', 'jhi' ),
		'parent_item'                => __( 'Parent Item', 'jhi' ),
		'parent_item_colon'          => __( 'Parent Item:', 'jhi' ),
		'new_item_name'              => __( 'New Location', 'jhi' ),
		'add_new_item'               => __( 'Add New Location', 'jhi' ),
		'edit_item'                  => __( 'Edit Location', 'jhi' ),
		'update_item'                => __( 'Update Location', 'jhi' ),
		'view_item'                  => __( 'View Location', 'jhi' ),
		'separate_items_with_commas' => __( 'Separate Locations with commas', 'jhi' ),
		'add_or_remove_items'        => __( 'Add or remove Locations', 'jhi' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'jhi' ),
		'popular_items'              => __( 'Popular Locations', 'jhi' ),
		'search_items'               => __( 'Search Locations', 'jhi' ),
		'not_found'                  => __( 'Not Found', 'jhi' ),
		'no_terms'                   => __( 'No Locations', 'jhi' ),
		'items_list'                 => __( 'Locations list', 'jhi' ),
		'items_list_navigation'      => __( 'Locations list navigation', 'jhi' ),
	);
	$rewrite = array(
		'slug'                       => 'location',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'query_var'                  => 'locations',
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'jhi_locations', array( 'jhi_services' ), $args );

}