<?php

add_action( 'init', 'register_jhi_services_post_type', 0 );
// Register Custom Post Type
function register_jhi_services_post_type() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'jhi' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'jhi' ),
		'menu_name'             => __( 'Services', 'jhi' ),
		'name_admin_bar'        => __( 'Services', 'jhi' ),
		'archives'              => __( 'Services Archives', 'jhi' ),
		'attributes'            => __( 'Services Attributes', 'jhi' ),
		'parent_item_colon'     => __( 'Service Item:', 'jhi' ),
		'all_items'             => __( 'All Services', 'jhi' ),
		'add_new_item'          => __( 'Add New Service', 'jhi' ),
		'add_new'               => __( 'Add Service', 'jhi' ),
		'new_item'              => __( 'New Service', 'jhi' ),
		'edit_item'             => __( 'Edit Service', 'jhi' ),
		'update_item'           => __( 'Update Service', 'jhi' ),
		'view_item'             => __( 'View Service', 'jhi' ),
		'view_items'            => __( 'View Services', 'jhi' ),
		'search_items'          => __( 'Search Service', 'jhi' ),
		'not_found'             => __( 'Service Not found', 'jhi' ),
		'not_found_in_trash'    => __( 'Service Not found in Trash', 'jhi' ),
		'featured_image'        => __( 'Service Featured Image', 'jhi' ),
		'set_featured_image'    => __( 'Set featured image', 'jhi' ),
		'remove_featured_image' => __( 'Remove featured image', 'jhi' ),
		'use_featured_image'    => __( 'Use as featured image', 'jhi' ),
		'insert_into_item'      => __( 'Insert into Service', 'jhi' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Service', 'jhi' ),
		'items_list'            => __( 'Services list', 'jhi' ),
		'items_list_navigation' => __( 'Services list navigation', 'jhi' ),
		'filter_items_list'     => __( 'Filter Services list', 'jhi' ),
	);
	$rewrite = array(
		'slug'                  => 'service',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Service', 'jhi' ),
		'description'           => __( 'Services', 'jhi' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'genesis-cpt-archives-settings', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => __( 'services', 'jhi' ),
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'jhi_services', $args );

}
